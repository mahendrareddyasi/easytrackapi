﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyTrackWebApi;

namespace EasyTrackWebApi.Controllers
{
    public class FlightModesController : ApiController
    {
        private decibelsoft_EasytrackEntities db = new decibelsoft_EasytrackEntities();

        // GET: api/FlightModes
        public IQueryable<FlightMode> GetFlightModes()
        {
            return db.FlightModes;
        }

        // GET: api/FlightModes/5
        [ResponseType(typeof(FlightMode))]
        public IHttpActionResult GetFlightMode(int id)
        {
            FlightMode flightMode = db.FlightModes.Find(id);
            if (flightMode == null)
            {
                return NotFound();
            }

            return Ok(flightMode);
        }

        // PUT: api/FlightModes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFlightMode(int id, FlightMode flightMode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != flightMode.Id)
            {
                return BadRequest();
            }

            db.Entry(flightMode).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FlightModeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/FlightModes
        [ResponseType(typeof(FlightMode))]
        public IHttpActionResult PostFlightMode(FlightMode flightMode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.FlightModes.Add(flightMode);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = flightMode.Id }, flightMode);
        }

        // DELETE: api/FlightModes/5
        [ResponseType(typeof(FlightMode))]
        public IHttpActionResult DeleteFlightMode(int id)
        {
            FlightMode flightMode = db.FlightModes.Find(id);
            if (flightMode == null)
            {
                return NotFound();
            }

            db.FlightModes.Remove(flightMode);
            db.SaveChanges();

            return Ok(flightMode);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FlightModeExists(int id)
        {
            return db.FlightModes.Count(e => e.Id == id) > 0;
        }
    }
}