﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyTrackWebApi;

namespace EasyTrackWebApi.Controllers
{
    public class PaymentsListsController : ApiController
    {
        private decibelsoft_EasytrackEntities db = new decibelsoft_EasytrackEntities();

        // GET: api/PaymentsLists
        public IQueryable<PaymentsList> GetPaymentsLists()
        {
            return db.PaymentsLists;
        }

        // GET: api/PaymentsLists/5
        [ResponseType(typeof(PaymentsList))]
        public IHttpActionResult GetPaymentsList(int id)
        {
            PaymentsList paymentsList = db.PaymentsLists.Find(id);
            if (paymentsList == null)
            {
                return NotFound();
            }

            return Ok(paymentsList);
        }

        // PUT: api/PaymentsLists/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPaymentsList(int id, PaymentsList paymentsList)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != paymentsList.ID)
            {
                return BadRequest();
            }

            db.Entry(paymentsList).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PaymentsListExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PaymentsLists
        [ResponseType(typeof(PaymentsList))]
        public IHttpActionResult PostPaymentsList(PaymentsList paymentsList)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PaymentsLists.Add(paymentsList);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = paymentsList.ID }, paymentsList);
        }

        // DELETE: api/PaymentsLists/5
        [ResponseType(typeof(PaymentsList))]
        public IHttpActionResult DeletePaymentsList(int id)
        {
            PaymentsList paymentsList = db.PaymentsLists.Find(id);
            if (paymentsList == null)
            {
                return NotFound();
            }

            db.PaymentsLists.Remove(paymentsList);
            db.SaveChanges();

            return Ok(paymentsList);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PaymentsListExists(int id)
        {
            return db.PaymentsLists.Count(e => e.ID == id) > 0;
        }
    }
}