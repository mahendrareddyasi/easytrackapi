﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyTrackWebApi;

namespace EasyTrackWebApi.Controllers
{
    public class TripDetailsController : ApiController
    {
        private decibelsoft_EasytrackEntities db = new decibelsoft_EasytrackEntities();

        // GET: api/TripDetails
        public IQueryable<TripDetail> GetTripDetails()
        {
            return db.TripDetails;
        }

        // GET: api/TripDetails/5
        [ResponseType(typeof(TripDetail))]
        public IHttpActionResult GetTripDetail(int id)
        {
            TripDetail tripDetail = db.TripDetails.Find(id);
            if (tripDetail == null)
            {
                return NotFound();
            }

            return Ok(tripDetail);
        }

        // GET: api/TripByDriverId/5
        public IList<TripDetail> GetTripByDriverId(int TripByDriverId)
        {
            return db.TripDetails.Where(x => x.driverID == TripByDriverId).ToList();
        }

        [ResponseType(typeof(TripDetail))]
        public IHttpActionResult UpdateTripEnd(int UserId, int TripId, int ClientId)
        {
            var res = db.TripDetails.Where(x => x.driverID == UserId && x.Client == ClientId && x.ID == TripId);
            res.FirstOrDefault().Status = "Completed";
            db.SaveChanges();
            return Ok(res);
        }

        public IList<TripDetail> GetTripByClientId(int TripByClientId)
        {
            return db.TripDetails.Where(x => x.Client == TripByClientId).ToList();
        }

        public IList<TripDetail> GetTripByClientTripId(int TripId, int ClientId)
        {
            return db.TripDetails.Where(x => x.Client == ClientId && x.ID == TripId).ToList();
        }

        // PUT: api/TripDetails/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTripDetail(int id, TripDetail tripDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tripDetail.ID)
            {
                return BadRequest();
            }

            db.Entry(tripDetail).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TripDetailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TripDetails
        [ResponseType(typeof(TripDetail))]
        public IHttpActionResult PostTripDetail(TripDetail tripDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TripDetails.Add(tripDetail);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tripDetail.ID }, tripDetail);
        }

        // DELETE: api/TripDetails/5
        [ResponseType(typeof(TripDetail))]
        public IHttpActionResult DeleteTripDetail(int id)
        {
            TripDetail tripDetail = db.TripDetails.Find(id);
            if (tripDetail == null)
            {
                return NotFound();
            }

            db.TripDetails.Remove(tripDetail);
            db.SaveChanges();

            return Ok(tripDetail);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TripDetailExists(int id)
        {
            return db.TripDetails.Count(e => e.ID == id) > 0;
        }
    }
}