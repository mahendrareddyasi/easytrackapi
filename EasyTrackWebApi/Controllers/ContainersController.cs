﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EasyTrackWebApi;

namespace EasyTrackWebApi.Controllers
{
    [Authorize]
    public class ContainersController : ApiController
    {
        private decibelsoft_EasytrackEntities db = new decibelsoft_EasytrackEntities();

        // GET: api/Containers
        public IQueryable<Container> GetContainers()
        {
            return db.Containers;
        }

        // GET: api/Containers/5
        [ResponseType(typeof(Container))]
        public async Task<IHttpActionResult> GetContainer(int id)
        {
            Container container = await db.Containers.FindAsync(id);
            if (container == null)
            {
                return NotFound();
            }

            return Ok(container);
        }

        // PUT: api/Containers/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutContainer(int id, Container container)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != container.ID)
            {
                return BadRequest();
            }

            db.Entry(container).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContainerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Containers
        [ResponseType(typeof(Container))]
        public async Task<IHttpActionResult> PostContainer(Container container)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Containers.Add(container);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = container.ID }, container);
        }

        // DELETE: api/Containers/5
        [ResponseType(typeof(Container))]
        public async Task<IHttpActionResult> DeleteContainer(int id)
        {
            Container container = await db.Containers.FindAsync(id);
            if (container == null)
            {
                return NotFound();
            }

            db.Containers.Remove(container);
            await db.SaveChangesAsync();

            return Ok(container);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ContainerExists(int id)
        {
            return db.Containers.Count(e => e.ID == id) > 0;
        }
    }
}