﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyTrackWebApi;

namespace EasyTrackWebApi.Controllers
{
    public class ShiftDetailsController : ApiController
    {
        private decibelsoft_EasytrackEntities db = new decibelsoft_EasytrackEntities();

        // GET: api/ShiftDetails
        public IQueryable<ShiftDetail> GetShiftDetails()
        {
            return db.ShiftDetails;
        }

        // GET: api/ShiftDetails/5
        [ResponseType(typeof(ShiftDetail))]
        public IHttpActionResult GetShiftDetail(int id)
        {
            ShiftDetail shiftDetail = db.ShiftDetails.Find(id);
            if (shiftDetail == null)
            {
                return NotFound();
            }

            return Ok(shiftDetail);
        }

        // PUT: api/ShiftDetails/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutShiftDetail(int id, ShiftDetail shiftDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != shiftDetail.ID)
            {
                return BadRequest();
            }

            db.Entry(shiftDetail).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShiftDetailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ShiftDetails
        [ResponseType(typeof(ShiftDetail))]
        public IHttpActionResult PostShiftDetail(ShiftDetail shiftDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ShiftDetails.Add(shiftDetail);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = shiftDetail.ID }, shiftDetail);
        }

        // DELETE: api/ShiftDetails/5
        [ResponseType(typeof(ShiftDetail))]
        public IHttpActionResult DeleteShiftDetail(int id)
        {
            ShiftDetail shiftDetail = db.ShiftDetails.Find(id);
            if (shiftDetail == null)
            {
                return NotFound();
            }

            db.ShiftDetails.Remove(shiftDetail);
            db.SaveChanges();

            return Ok(shiftDetail);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ShiftDetailExists(int id)
        {
            return db.ShiftDetails.Count(e => e.ID == id) > 0;
        }
    }
}