﻿using EasyTrackWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace EasyTrackWebApi.Controllers
{
    public class LoginController : ApiController
    {
        private decibelsoft_EasytrackEntities db = new decibelsoft_EasytrackEntities();

        [HttpGet]
        [ActionName("FindByUser")]
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> GetAuthentication(string username, string password)
        {
            User result = db.Users.SingleOrDefault(x => x.LastName == username && x.Password == password);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

    }
}
